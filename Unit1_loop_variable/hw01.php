
<!DOCTYPE html>
<html>
    <head>
        <title>nguyenhiepvan.bka homework php</title>
        <meta charset="UTF-8">
        <meta name="nguyenhiepvan" content="PHP beginner, zent.edu.vn">
    </head>
    <body>
        <h1>Homework PHP - HW01</h1>
        <h2><Strong><u>Đề bài: </u></Strong></h2> <h3>Viết chương trình tính tổng S = 1 /1! + 2 /2! + ....+ n / n! ?</h3>
        <br>
        <form method="get" action="hw01.php">
            <input type="number" name="input" value="0"/>
            <input type="submit" name="go" value="caculate"/>
        </form>
        <br>
        <h2><?php

// hàm tính giai thừa
            function factorial($x) {
                if ($x < 3)
                    return $x;
                else
                    return $x * factorial($x - 1);
            }

            if (!empty($_GET['go'])) {
                $n = isset($_GET['input']) ? (int) $_GET['input'] : 0;

                if ($n < 1)
                    echo "số đã nhập không hợp lệ";
                else {
                    $sum = 1;
                    for ($i = 2; $i <= $n; $i++) {
                        $sum = $sum + 1 / factorial($i - 1);
                    }

                    echo "S = " . $sum;
                }
            }
            ?></h2>
    </body>
</html>
