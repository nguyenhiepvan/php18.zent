
<!DOCTYPE html>
<html>
    <head>
        <title>nguyenhiepvan.bka homework php</title>
        <meta charset="UTF-8">
        <meta name="nguyenhiepvan" content="PHP beginner, zent.edu.vn">
    </head>
    <body>
        <h1>Homework PHP - HW09</h1>
        <h2><Strong><u>Đề bài: </u></Strong></h2> <h3>Viết chương trình giải phương trình bậc nhất ax + b = 0  </h3>
        <br>
        <br>
        <form method="get" action="hw08.php">
            a = <input type="number" name="a" value="0"/>
            <br>
            <br>
            b = <input type="number" name="b" value="0"/>
            <br>
            <br>
            <input type="submit" name="go" value="caculate"/>
        </form>
        <br>
        <h2>
            <?php
            if (!empty($_GET['go'])) {
                $a = isset($_GET['a']) ? (int) $_GET['a'] : 0;
                $b = isset($_GET['b']) ? (int) $_GET['b'] : 0;

                if ($a == 0) {
                    if ($b == 0)
                        echo "<br> Phương trình vô số nghiệm";
                    else
                        echo "<br> Phương trình vô nghiệm";
                } else
                    echo "<br> Phương trình có nghiệm x = " . $b * (-1) / $a;
            }
            ?>
        </h2>
    </body>
</html>
