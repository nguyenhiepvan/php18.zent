
<!DOCTYPE html>
<html>
    <head>
        <title>nguyenhiepvan.bka homework php</title>
        <meta charset="UTF-8">
        <meta name="nguyenhiepvan" content="PHP beginner, zent.edu.vn">
    </head>
    <body>
        <h1>Homework PHP - HW07</h1>
        <h2><Strong><u>Đề bài: </u></Strong></h2> <h3>In ra bảng cửu chương sử dụng vòng lặp for </h3>
        <br>
        <h2>
            <?php
            for ($i = 1; $i <= 10; $i++) {
                for ($j = 1; $j <= 9; $j++) {
                    echo "" . $j . "x" . $i . " = " . $i * $j;
                    echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    if (strlen((string) $i * $j) == 1)
                        echo "&nbsp;&nbsp;";
                    if (strlen((string) $i) == 1)
                        echo "&nbsp;&nbsp;";
                }
                echo "<br> ";
            }
            ?>
        </h2>
    </body>
</html>
