<?php
/**
* lớp sản phẩm bao gồm các thông tin 
*Thuộc tính: Mã sản phẩm, tên sản phẩm, số lượng, đơn giá, hãng sản xuất
*Phương thức: In thông tin sản phẩm
*/
class Product
{
	var $ma_san_pham;
	var $ten_san_pham;
	var $so_luong;
	var $don_gia;
	var $hang_san_xuat;

	function __construct($ma_san_pham,$ten_san_pham,$so_luong,$don_gia,$hang_san_xuat)
	{
		$this->ma_san_pham = $ma_san_pham;
		$this->ten_san_pham = $ten_san_pham;
		$this->so_luong = $so_luong;
		$this->don_gia = $don_gia;
		$this->hang_san_xuat = $hang_san_xuat;
	}

	function display()
	{
		echo "Thông tin sản phẩm: " . "<br>";
		echo "Mã sản phẩm: " . $this->ma_san_pham . "<br>";
		echo "Tên sản phẩm: " . $this->ten_san_pham . "<br>";
		echo "Số lượng: " . $this->so_luong . "<br>";
		echo "Đơn giá: " . $this->don_gia . "<br>";
		echo "Hãng sản xuất: " . $this->hang_san_xuat . "<br>";
	}
}

$product_1 = new Product("sp01","product_1",10,100000,"VN");
$product_2 = new Product("sp02","product_2",10,100000,"VN");
$product_3 = new Product("sp03","product_3",10,100000,"VN");

$product_1->display();
$product_2->display();
$product_3->display();
