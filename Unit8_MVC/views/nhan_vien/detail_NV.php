<?php 
include_once('layouts/header.php');
 ?>
</ol>

<div class="container">
	<h1>Thông tin nhân viên</h1>
	<ul>
		<li style="overflow: auto;">
			<img src="public/images/staff/<?=$row['PHOTOS']?>" width = "100px" height = "100px" style= "float:left">
		</li>
		<li>Mã nhân viên: <?=$row['CODE']?></li>
		<li>Họ và tên: <?=$row['NAME']?></li>
		<li>Số điện thoại: <?=$row['PHONE_NUMBER']?></li>
		<li>Email: <?=$row['EMAIL']?></li>
		<li>Địa chỉ: <?=$row['ADRESS']?></li>
		<li>Ngày sinh: <?= $date?></li>
		<li>Loại nhân viên: <?=$row['NAME_TYPE']?></li>
		<li>Mã loại: <?=$row['CODE_TYPE']?></li>
	</ul>
</div>
<?php include_once('layouts/footer.php') ?>