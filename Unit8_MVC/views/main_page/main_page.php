<?php include_once('layouts/header.php') ?>
<div class="container">
	<div class="table-responsive">
		<?php 
		if(isset($_COOKIE['msg'])) {echo $_COOKIE['msg'] ;}
		if(isset($_COOKIE['msg_error'])) {echo $_COOKIE['msg_error'] ;}
		?>
		<?php if(isset($_GET['id']) == false || $_GET['id'] == 'nhan-vien')
		{ $i = 0; ?>
		
		<h2 align="center">DANH SÁCH NHÂN VIÊN</h2>
		<hr>
		<a href="NV_add.php" class="btn btn-primary">Thêm mới</a>
		<table class="table-dark table-striped table-hover"  width= 100%>
			<thead>
				<tr >
					<th></th>
					<th>Họ và tên</th>
					<th>email</th>
					<th>Số điện thoại</th>
					<th>Loại nhân viên</th>		
					<th>#</th>			
				</tr>
			</thead>

			<tbody>

				<?php foreach ($result as $row) 
				{
					$i++;
					?>

					<tr>

						<td><a href="?mod=nhan_vien&act=detail&code=<?= $row['CODE']?>"><?=$i?></a></td>
						<td><a href="?mod=nhan_vien&act=detail&code=<?= $row['CODE']?>"><?= $row['NAME']?></a></td>
						<td><a href="?mod=nhan_vien&act=detail&code=<?= $row['CODE']?>"><?= $row['EMAIL']?></a></td>
						<td><a href="?mod=nhan_vien&act=detail&code=<?= $row['CODE']?>"><?= $row['PHONE_NUMBER']?></a></td>
						<td><a href="?mod=nhan_vien&act=detail&code=<?= $row['CODE']?>"><?= $row['NAME_TYPE']?></a></td>
						<td><a href="NV_update.php?CODE=<?= $row['CODE']?>" class="btn btn-warning">Update</a>  
							<a href="NV_delete.php?CODE=<?= $row['CODE']?>" class="btn btn-danger">Delete</a></td>

						</tr>
						<?php 	
					}?>
				</tbody>
			</table>
			<?php } ?>
			<?php if(isset($_GET['id']) && $_GET['id'] == 'khach-hang')
			{ $i =0; ?>

			<h2 align="center">DANH SÁCH KHÁCH HÀNG</h2>
			<hr>
			<a href="?mod=khach_hang&act=add" class="btn btn-primary">Thêm mới</a>
			<table class="table-dark table-striped table-hover"  width= 100%>
				<thead>
					<tr >
						<th></th>
						<th>Họ và tên</th>
						<th>email</th>
						<th>Số điện thoại</th>
						<th>Địa chỉ</th>
						<th>#</th>					
					</tr>
				</thead>

				<tbody>

					<?php foreach ($result as $row) {
						$i++;
						?>
						<tr>
							<td><a href="?mod=khach_hang&act=detail&code=<?= $row['CODE']?>"><?=$i?></a></td>
							<td><a href="?mod=khach_hang&act=detail&code=<?= $row['CODE']?>"><?= $row['NAME']?></a></td>
							<td><a href="?mod=khach_hang&act=detail&code=<?= $row['CODE']?>"><?= $row['EMAIL']?></a></td>
							<td><a href="?mod=khach_hang&act=detail&code=<?= $row['CODE']?>"><?= $row['PHONE_NUMBER']?></a></td>
							<td><a href="?mod=khach_hang&act=detail&code=<?= $row['CODE']?>"><?= $row['ADRESS']?></a></td>

							<td><a  href="?mod=khach_hang&act=edit&code=<?= $row['CODE']?>" class="btn btn-warning">Update</a>  

							<a href="?mod=khach_hang&act=delete&code=<?= $row['CODE']?>" class="btn btn-danger">Delete</a></td>
							</tr>
							<?php 	
						}?>
					</tbody>
				</table>
				<?php } ?>

			</div>
		</div>