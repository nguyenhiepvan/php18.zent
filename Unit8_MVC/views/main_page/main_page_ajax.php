<?php include_once('layouts/header.php') ?>

<h2 align="center"><span id="title"></span></h2>
<hr>
<button type="button" name="add" id="add" class="btn btn-success btn-xs">Thêm mới</button>
<div class="table-responsive" id="user_data">
</div>
<?php if(isset($_GET['id']) == false || $_GET['id'] == 'nhan-vien')
{ ?>
	<script type="text/javascript">
		$('#title').text("DANH SÁCH NHÂN VIÊN");
		$('#add').val('insert_NV');
	</script>
	<?php } ?>
<?php if(isset($_GET['id']) == false || $_GET['id'] == 'khach-hang')
	{ ?>
	<script type="text/javascript">
		$('#title').text("DANH SÁCH KHÁCH HÀNG");
		$('#add').val('insert_KH');
	</script>
	<?php } ?>
<?php echo $result; ?>

	<!--modal add KH-->
	<div class="modal fade" id="add_KH" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Thêm khách hàng mới</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form method="POST" id="user_form">
						<div class="form-group" style="overflow: hidden;">
							<img id="AVATAR" src="public/images/guess/untitled.png" width = "100px" height = "100px" style= "float:left">
							<form action="#" method="post" enctype="multipart/form-data" style="float: left; padding-left: 30px;padding-top: 30px;">

								<input type="file" class="form-control" name="PHOTOS" style="width: 150px">
								<input type="submit" value="Chose this photo" name="submit">
							</form>
							<span id="error_name" class="text-danger"></span>
						</div>
						<div class="form-group">
							<label>Họ và tên:</label>
							<input type="text" name="NAME" id="NAME" class="form-control"/>
							<span id="error_name" class="text-danger"></span>
						</div>
						<div class="form-group">
							<label>Số điện thoại:</label>
							<input type="text" name="PHONE_NUMBER" id="PHONE_NUMBER" class="form-control" />
							<span id="error_name" class="text-danger"></span>
						</div>
						<div class="form-group">
							<label>Email:</label>
							<input type="text" name="EMAIL" id="EMAIL" class="form-control" />
							<span id="error_name" class="text-danger"></span>
						</div>
						<div class="form-group">
							<label>Địa chỉ:</label>
							<input type="text" name="ADRESS" id="ADRESS" class="form-control" />
							<span id="error_name" class="text-danger"></span>
						</div>
						<div class="form-group">
							<label>Ngày sinh:</label>
							<input type="text" name="DATE_OF_BIRTH" id="DATE_OF_BIRTH" class="form-control" />
							<span id="error_name" class="text-danger"></span>
						</div>

					</form>
				</div>
				<div class="modal-footer">
					<div class="form-group">
						<input type="hidden" name="action" id="action" value="insert" />
						<input type="hidden" name="hidden_id" id="hidden_id" />
						<input type="submit" name="form_action" id="form_action" class="btn btn-info" value="Insert" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--modal update-->
<script type="text/javascript">
	$(document).ready(function(){
		<?php if(isset($_GET['id']) == false || $_GET['id'] == 'nhan-vien')
		{ ?>
			

		<?php } ?>
		<?php if(isset($_GET['id']) && $_GET['id'] == 'khach-hang')
		{ ?>
			
				$('#add').click(function(){

					$('#add_KH').attr('title', 'Thêm khách hàng mới');
					$('#action').val('insert_KH');
					$('#form_action').val('Thêm mới');
					$('#user_form')[0].reset();
					$('#form_action').attr('disabled', false);
					$("#add_KH").modal('show');
				});

			<?php } ?>

		});
	</script>
	<?php include_once('layouts/footer.php') ?>