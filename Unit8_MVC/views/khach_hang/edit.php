<?php include_once('layouts/header.php'); ?>
<div class="container">
    	<h3 align="center">CẬP NHẬT KHÁCH HÀNG</h3>
    	<hr>

    	<div style="overflow: auto;">
    		<img src="public/images/guess/<?=$_SESSION ['guess'] ['PHOTOS'] ?>" width = "100px" height = "100px" style= "float:left">
    		<form action="?mod=khach_hang&act=upload_photos&code=<?=$row['CODE']?>" method="post" enctype="multipart/form-data" style="float: left; padding-left: 30px;padding-top: 30px;">

    			<input type="file" class="form-control" name="PHOTOS" style="width: 150px">
    			<input type="submit" value="Chose this photo" name="submit">
    		</form>

    	</li>
    </div>	
    <form action="?mod=khach_hang&act=update" method="POST" role="form" enctype="multipart/form-data">
    	<div class="form-group">
    		<label for="">Mã khách hàng</label>
    		<input type="text" class="form-control" id="" value="<?=$row['CODE']?>" name="CODE" readonly>
    	</div>
    	<div class="form-group">
    		<label for="">Tên Khách hàng</label>
    		<input type="text" class="form-control" id="" value="<?=$row['NAME']?>" name="NAME">
    	</div>  
    	<div class="form-group">
    		<label for="">Số điện thoại</label>
    		<input type="number" class="form-control" id="" value="<?=$row['PHONE_NUMBER']?>" name="PHONE_NUMBER">
    	</div>
    	<div class="form-group">
    		<label for="">email</label>
    		<input type="email" class="form-control" id="" value="<?=$row['EMAIL']?>" name="EMAIL">
    	</div>
    	<div class="form-group">
    		<label for="">Địa chỉ</label>
    		<input type="text" class="form-control" id="" value="<?=$row['ADRESS']?>" name="ADRESS">
    	</div>
    	<div class="form-group">
    		<label for="">Ngày sinh</label>
    		<input type="date" data-date="" data-date-format="yyyy-MM-dd" value="<?=$row['DATE_OF_BIRTH']?>" class="form-control" id="" name="DATE_OF_BIRTH">
    	</div>

    	<button type="submit" class="btn btn-primary">Cập nhật thông tin</button>
    </form>
</div>

<?php include_once('layouts/footer.php') ?>