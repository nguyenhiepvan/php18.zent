<?php 
/**
* 
*/
require_once('models/nhan_vien.php');
require_once('models/khach_hang.php');
class controller_main
{
	public function All_NV()
	{
		$staff = new nhan_vien();
		$result = $staff->All();

		require_once('views/main_page/main_page.php');
	}
	public function All_KH()
	{
		$cus = new khach_hang();
		$result = $cus->All();
		require_once('views/main_page/main_page.php');
	}
}
?>