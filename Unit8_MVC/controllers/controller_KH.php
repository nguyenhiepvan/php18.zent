<?php 
/**
* 
*/

require_once('models/khach_hang.php');
session_start();
class controller_KH
{
	
	function detail($code){
		$cus = new khach_hang();
		$row = $cus->find($code);
		$dt = new DateTime($row['DATE_OF_BIRTH']);
		$date = $dt->format('d-m-Y');
		require_once('views/khach_hang/detail_KH.php');
	}

	function edit($code){
		$cus = new khach_hang();
		$row = $cus->find($code);
		
		
		if(!isset($_SESSION ['guess'] ['PHOTOS'])){
			
			$_SESSION ['guess'] ['PHOTOS'] = $row['PHOTOS'];	}
		//var_dump($_SESSION ['guess'] ['PHOTOS']);
		//die;
			require_once('views/khach_hang/edit.php');
		}

		function update()
		{
			require_once('public/connection_database.php');
			$query = "UPDATE khach_hang
			SET NAME = '".$_POST['NAME']."',PHONE_NUMBER = '".$_POST['PHONE_NUMBER']."',EMAIL = '".$_POST['EMAIL']."',
			ADRESS = '".$_POST['ADRESS']."', DATE_OF_BIRTH = '".$_POST['DATE_OF_BIRTH']."', PHOTOS = '".$_SESSION ['guess'] ['PHOTOS']."'
			WHERE CODE = '".$_POST['CODE']."';";
			$statement = $conn->prepare($query);
			$result = $statement->execute();

			if($result)
			{
				setcookie("msg","Cập nhật thành công!",time()+2);
				session_destroy();
			//header("Location:index.php?id=khach-hang")
			}
			else setcookie("msg_error","Có gì đó sai sai!",time()+2);
			header("Location:index.php?id=khach-hang");
		}

		function upload_photos()
		{
			require_once('public/file_upload.php');

		 if(isset($_POST['submit'])){ // kiểm tra xem button Submit đã được click hay chưa ? 
		 	$uploads = file_upload("public/images/guess","PHOTOS",500000,array( 'JPG', 'JPEG', 'GIF', 'PNG' , 'SVG','jpg', 'jpeg', 'gif', 'png' , 'svg'));
		 	if(gettype($uploads) == "array"){
        print_r($uploads); // Trả về mảng lỗi nếu ko upload được
    }else{

	        $_SESSION ['guess'] ['PHOTOS'] = $uploads; // Trả về tên file nếu upload thành công
	        $this->edit($_GET['code']);
	    }

	}

}


function add(){
	$_SESSION ['guess'] ['PHOTOS'] = "untitled.png";
	require_once('views/khach_hang/add.php');
}

function insert()
{
	require_once('public/connection_database.php');
	require_once('public/generate_id.php');
	$data = $_POST;
	$id = "KH-" .getToken(8);
	$photo = isset($_SESSION['guess']['PHOTOS'])?$_SESSION['guess']['PHOTOS']:"untitled.png";
	$query = "INSERT INTO khach_hang (
	CODE,
	NAME,
	PHONE_NUMBER,
	EMAIL,
	ADRESS,
	DATE_OF_BIRTH,
	PHOTOS
)
VALUES
(
	'".$id."',
	'".$data['NAME']."',
	'".$data['PHONE_NUMBER']."',
	'".$data['EMAIL']."',
	'".$data['ADRESS']."',
	'".$data['DATE_OF_BIRTH']."',
	'".$photo."'
	);";
	$statement = $conn->prepare($query);
	$result = $statement->execute();
	if($result)
	{
		setcookie("msg","thêm thành công!",time()+2);
	}
	else setcookie("msg_error","Có gì đó sai sai!",time()+2);
	session_destroy();
	header("Location: index.php?id=khach-hang");
}

function delete()
{
	require_once('public/connection_database.php');

	$query = "DELETE FROM khach_hang WHERE CODE = '".$_GET['code']."'";
	$statement = $conn->prepare($query);
	$result = $statement->execute();
	if($result)
	{
		setcookie("msg","Xóa thành công!",time()+2);
	}
	else setcookie("msg_error","Có gì đó sai sai!",time()+2);
	header("Location: index.php?id=khach-hang");
}

}
?>