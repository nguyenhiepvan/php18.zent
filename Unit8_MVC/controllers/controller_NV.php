<?php 
/**
* 
*/

require_once('models/nhan_vien.php');
class controller_NV
{
	function detail($code){
		$staff = new nhan_vien();
		$row = $staff->find($code);
		$dt = new DateTime($row['DATE_OF_BIRTH']);
		$date = $dt->format('d-m-Y');
		require_once('views/nhan_vien/detail_NV.php');
	}
}
?>