<?php 
include_once('connection_database.php');
$query = "SELECT
nv.`CODE`,
nv.`NAME`,
t.`NAME_TYPE`,
nv.`PHONE_NUMBER`,
nv.EMAIL
FROM
nhan_vien nv
JOIN type_nhan_vien t ON nv.TYPE = t.`CODE_TYPE`";
$statement = $conn->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
$total_row = $statement->rowCount();
$output = '
<table class="table-dark table-striped table-hover"  width= 100%>
	<thead>
		<tr >
			<th>ID</th>
			<th>Họ và tên</th>
			<th>email</th>
			<th>Số điện thoại</th>
			<th>Loại nhân viên</th>		
			<th>#</th>			
		</tr>
	</thead>
	';
	if($total_row > 0)
	{
		foreach($result as $row)
		{
			$output .= '
			<tr>
				<td><a href="NV_profile.php?CODE='.$row['CODE'].'">'.$row['CODE'].'</a></td>
				<td><a href="NV_profile.php?CODE='.$row['CODE'].'">'.$row['NAME'].'</a></td>
				<td><a href="NV_profile.php?CODE='.$row['CODE'].'">'.$row['EMAIL'].'</a></td>
				<td><a href="NV_profile.php?CODE='.$row['CODE'].'">'.$row['PHONE_NUMBER'].'</a></td>
				<td><a href="NV_profile.php?CODE='.$row['CODE'].'">'.$row['NAME_TYPE'].'</a></td>
				
				<td><a href="NV_update.php?CODE='.$row['CODE'].'" class="btn btn-warning">Update</a>  
					<a href="NV_delete.php?CODE='.$row['CODE'].'" class="btn btn-danger">Delete</a></td>
				</tr>
				';
			}
		}
		else
		{
			$output .= '
			<tr>
				<td colspan="4" align="center">Data not found</td>
			</tr>
			';
		}
		$output .= '</table>';
		echo $output;

		?>
