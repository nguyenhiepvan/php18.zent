<?php
include_once('layouts/header.php');
include_once('connection_database.php');

// Cau lenh truy van co so du lieu

	$code = $_GET['CODE'];
	$query = "SELECT *
	FROM
	nhan_vien nv
	JOIN type_nhan_vien t ON nv.TYPE = t.`CODE_TYPE`
	WHERE
	nv.`CODE` = '".$code."'";

	// Thuc thi cau lenh truy van co so du lieu
	$result = $conn->query($query);

	$row = $result->fetch_assoc();
	
	$dt = new DateTime($row['DATE_OF_BIRTH']);
	$date = $dt->format('d-m-Y');
?>
</ol>

<div class="container">
	<h1>Thông tin nhân viên</h1>
	<ul>
		<li style="overflow: auto;">
			<img src="images/staff/<?=$row['PHOTOS']?>" width = "100px" height = "100px" style= "float:left">
		</li>
		<li>Mã nhân viên: <?=$row['CODE']?></li>
		<li>Họ và tên: <?=$row['NAME']?></li>
		<li>Số điện thoại: <?=$row['PHONE_NUMBER']?></li>
		<li>Email: <?=$row['EMAIL']?></li>
		<li>Địa chỉ: <?=$row['ADRESS']?></li>
		<li>Ngày sinh: <?= $date?></li>
		<li>Loại nhân viên: <?=$row['NAME_TYPE']?></li>
		<li>Mã loại: <?=$row['CODE_TYPE']?></li>
	</ul>
</div>
<?php include_once('layouts/footer.php') ?>