<?php include_once('layouts/header.php') ?>
<?php 
include_once('connection_database.php');

// Cau lenh truy van co so du lieu
if(isset($_GET['id']) && $_GET['id'] == 'khach-hang')
{
	
	$query = "SELECT *
	FROM khach_hang";

	// Thuc thi cau lenh truy van co so du lieu
	$result = $conn->query($query);
	
	
}else{
	$query = "SELECT
	nv.`CODE`,
	nv.`NAME`,
	t.`NAME_TYPE`,
	nv.`PHONE_NUMBER`,
	nv.EMAIL
	FROM
	nhan_vien nv
	JOIN type_nhan_vien t ON nv.TYPE = t.`CODE_TYPE`";
}
	// Thuc thi cau lenh truy van co so du lieu
$result = $conn->query($query);

?>


<div class="container">


	<div class="table-responsive" id="user_data">
		<?php 
		if(isset($_COOKIE['msg'])) {echo $_COOKIE['msg'] ;}
		if(isset($_COOKIE['msg_error'])) {echo $_COOKIE['msg_error'] ;}
		?>
		<?php if(isset($_GET['id']) == false || $_GET['id'] == 'nhan-vien')
		{ ?>
		
		<h2 align="center">Tite</h2>
		<hr>
		<a href="NV_add.php" class="btn btn-primary">Thêm mới</a>
		<table class="table-dark table-striped table-hover"  width= 100%>
			<thead>
				<tr >
					<th>ID</th>
					<th>Họ và tên</th>
					<th>email</th>
					<th>Số điện thoại</th>
					<th>Loại nhân viên</th>		
					<th>#</th>			
				</tr>
			</thead>

			<tbody>

				<?php while ($row = $result->fetch_assoc()) {
					?>

					<tr>

						<td><a href="NV_profile.php?CODE=<?= $row['CODE']?>"><?= $row['CODE']?></a></td>
						<td><a href="NV_profile.php?CODE=<?= $row['CODE']?>"><?= $row['NAME']?></a></td>
						<td><a href="NV_profile.php?CODE=<?= $row['CODE']?>"><?= $row['EMAIL']?></a></td>
						<td><a href="NV_profile.php?CODE=<?= $row['CODE']?>"><?= $row['PHONE_NUMBER']?></a></td>
						<td><a href="NV_profile.php?CODE=<?= $row['CODE']?>"><?= $row['NAME_TYPE']?></a></td>
						<td><a href="NV_update.php?CODE=<?= $row['CODE']?>" class="btn btn-warning">Update</a>  
							<a href="NV_delete.php?CODE=<?= $row['CODE']?>" class="btn btn-danger">Delete</a></td>

						</tr>
						<?php 	
					}?>
				</tbody>
			</table>
			<?php } ?>
			<?php if(isset($_GET['id']) && $_GET['id'] == 'khach-hang')
			{ ?>

			<h2 align="center">DANH SÁCH KHÁCH HÀNG</h2>
			<hr>
			<a href="KH_add.php" class="btn btn-primary">Thêm mới</a>
			<table class="table-dark table-striped table-hover"  width= 100%>
				<thead>
					<tr >
						<th>ID</th>
						<th>Họ và tên</th>
						<th>email</th>
						<th>Số điện thoại</th>
						<th>Địa chỉ</th>
						<th>#</th>					
					</tr>
				</thead>

				<tbody>

					<?php while ($row = $result->fetch_assoc()) {
						?>
						<tr>
							<td><a  href="javascript:;" data-id="<?= $row['CODE']?>" class="show" data-toggle="modal" data-target="#show_detail"><?= $row['CODE']?></a></td>
							<td><a  href="javascript:;" data-id="<?= $row['CODE']?>" class="show" data-toggle="modal" data-target="#show_detail"><?= $row['NAME']?></a></td>
							<td><a  href="javascript:;" data-id="<?= $row['CODE']?>" class="show" data-toggle="modal" data-target="#show_detail"><?= $row['EMAIL']?></a></td>
							<td><a  href="javascript:;" data-id="<?= $row['CODE']?>" class="show" data-toggle="modal" data-target="#show_detail"><?= $row['PHONE_NUMBER']?></a></td>
							<td><a  href="javascript:;" data-id="<?= $row['CODE']?>" class="show" data-toggle="modal" data-target="#show_detail"><?= $row['ADRESS']?></a></td>

							<td><a  href="javascript:;" data-id="<?= $row['CODE']?>" class="btn btn-warning update" data-toggle="modal" data-target="#update">Update</a>  
								<td><a href="KH_delete.php?CODE=<?= $row['CODE']?>" class="btn btn-danger">Delete</a></td>
							</tr>
							<?php 	
						}?>
					</tbody>
				</table>
				<?php } ?>

			</div>
		</div>
		<!-- Modal detail-->
		<div class="modal fade" id="show_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLongTitle">Thông tin khách hàng</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<img style="height: 100px;width: 100px;" id="PHOTOS_">
						<p>Tên khách hàng: <span id="NAME_"></span> </p>
						<p>Số điện thoại: <span id="PHONE_NUMBER_"></span></p>
						<p>email: <span id="EMAIL_"></span></p>
						<p>Địa chỉ: <span id="ADRESS_"></span></p>
						<p>Ngày sinh: <span id="DATE_OF_BIRTH_"></span></p>					
					</div>
					
				</div>
			</div>
		</div>
		<!-- Modal detail-->

		<!--modal update-->
		<div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form method="POST">
						<div class="form-group">
								<img style="height: 100px;width: 100px;" id="PHOTOS">
								<span id="error_name" class="text-danger"></span>
							</div>
							<div class="form-group">
								<label>Họ và tên:</label>
								<input type="text" name="NAME" id="NAME" class="form-control"/>
								<span id="error_name" class="text-danger"></span>
							</div>
							<div class="form-group">
								<label>Số điện thoại:</label>
								<input type="text" name="PHONE_NUMBER" id="PHONE_NUMBER" class="form-control" />
								<span id="error_name" class="text-danger"></span>
							</div>
							<div class="form-group">
								<label>Email:</label>
								<input type="text" name="EMAIL" id="EMAIL" class="form-control" />
								<span id="error_name" class="text-danger"></span>
							</div>
							<div class="form-group">
								<label>Địa chỉ:</label>
								<input type="text" name="ADRESS" id="ADRESS" class="form-control" />
								<span id="error_name" class="text-danger"></span>
							</div>
							<div class="form-group">
								<label>Ngày sinh:</label>
								<input type="text" name="DATE_OF_BIRTH" id="DATE_OF_BIRTH" class="form-control" />
								<span id="error_name" class="text-danger"></span>
							</div>
							
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary">Save changes</button>
					</div>
				</div>
			</div>
		</div>
		<!--modal update-->
		<script type="text/javascript">
		$(document).on('click','.show',function(){
			var id = $(this).data('id');
			$.ajax({
				url:'KH_detail.php?CODE='+id,
				type : 'get',
				dataType : 'json',
				success: function(result){
					$('#PHOTOS_').attr('src','images/guess/'+result.PHOTOS);
					$('#NAME_').html(result.NAME);
					$('#PHONE_NUMBER_').html(result.PHONE_NUMBER);
					$('#EMAIL_').html(result.EMAIL);
					$('#ADRESS_').html(result.ADRESS);

						//var dt = new DateTime();
						//var date = dt->format('d-m-Y');
						$('#DATE_OF_BIRTH_').html(result.DATE_OF_BIRTH);

						$('#show_detail').modal('show');
					}
				});


		});
		$(document).on('click','.update',function(){
			var id = $(this).data('id');
			$.ajax({
				url:'KH_detail.php?CODE='+id,
				method:'POST',
				dataType : 'json',
				success: function(result){
					console.log(result);
					$('#PHOTOS').attr('src','images/guess/'+result.PHOTOS);
					$('#NAME').val(result.NAME);
					$('#PHONE_NUMBER').val(result.PHONE_NUMBER);
					$('#EMAIL').val(result.EMAIL);
					$('#ADRESS').val(result.ADRESS);

						//var dt = new DateTime();
						//var date = dt->format('d-m-Y');
						$('#DATE_OF_BIRTH').val(result.DATE_OF_BIRTH);

						$('#update').modal('show');
					}
				});

		});
		</script>
		<?php include_once('layouts/footer.php') ?>
