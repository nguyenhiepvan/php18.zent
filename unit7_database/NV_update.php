<?php 
session_start();
include_once('layouts/header.php');
include_once('file_upload.php');
include_once('connection_database.php');

$query = "SELECT *
FROM
nhan_vien nv
JOIN type_nhan_vien t ON nv.TYPE = t.`CODE_TYPE`
WHERE
nv.`CODE` = '".$_GET['CODE']."'";

$result = $conn->query($query);
$row = $result->fetch_assoc();

$query1 = "SELECT NAME_TYPE FROM type_nhan_vien";
$result1 = $conn->query($query1);

$_SESSION['staff']['PHOTOS'] = $row['PHOTOS'];

  if(isset($_POST['submit'])){ // kiểm tra xem button Submit đã được click hay chưa ? 
  	$uploads = file_upload("images/staff","PHOTOS",500000,array( 'JPG', 'JPEG', 'GIF', 'PNG' , 'SVG','jpg', 'jpeg', 'gif', 'png' , 'svg'));
  	if(gettype($uploads) == "array"){
            print_r($uploads); // Trả về mảng lỗi nếu ko upload được
        }else{

            $_SESSION ['staff'] ['PHOTOS'] = $uploads; // Trả về tên file nếu upload thành công
        }
    }
    ?>
    <div class="container">
    	<h3 align="center">CẬP NHẬT NHÂN VIÊN</h3>
    	<hr>

    	<div style="overflow: auto;">
    		<img src="images/staff/<?=$_SESSION ['staff'] ['PHOTOS']?>" width = "100px" height = "100px" style= "float:left">
    		<form action="#" method="post" enctype="multipart/form-data" style="float: left; padding-left: 30px;padding-top: 30px;">

    			<input type="file" class="form-control" name="PHOTOS" style="width: 150px">
    			<input type="submit" value="Chose this photo" name="submit">
    		</form>

    	</li>
    </div>	
    <form action="NV_update_process.php" method="POST" role="form" enctype="multipart/form-data">
    	<div class="form-group">
    		<label for="">Mã Nhân viên</label>
    		<input type="text" class="form-control" id="" value="<?=$row['CODE']?>" name="CODE" readonly>
    	</div>
    	<div class="form-group">
    		<label for="">Tên Nhân viên</label>
    		<input type="text" class="form-control" id="" value="<?=$row['NAME']?>" name="NAME">
    	</div>  
    	<div class="form-group">
    		<label for="">Số điện thoại</label>
    		<input type="number" class="form-control" id="" value="<?=$row['PHONE_NUMBER']?>" name="PHONE_NUMBER">
    	</div>
    	<div class="form-group">
    		<label for="">email</label>
    		<input type="email" class="form-control" id="" value="<?=$row['EMAIL']?>" name="EMAIL">
    	</div>
    	<div class="form-group">
    		<label for="">Địa chỉ</label>
    		<input type="text" class="form-control" id="" value="<?=$row['ADRESS']?>" name="ADRESS">
    	</div>
    	<div class="form-group">
    		<label for="">Ngày sinh</label>
    		<input type="date" data-date="" data-date-format="yyyy-MM-dd" value="<?=$row['DATE_OF_BIRTH']?>" class="form-control" id="" name="DATE_OF_BIRTH">
    	</div>
    	<div class="form-group">
    		<label for="">Loại nhân viên</label>
    		<select name="TYPE">
    			<?php while ($row1 = $result1->fetch_assoc()) {
    				if($row1['NAME_TYPE'] == $row['NAME_TYPE']){
    					?>
    					<option value="<?=$row1['NAME_TYPE']?>" selected ><?=$row1['NAME_TYPE']?></option>
    					<?php } else{?>
    					<option value="<?=$row1['NAME_TYPE']?>" ><?=$row1['NAME_TYPE']?></option>
    					<?php }} ?>
    				</select> 
    			</div>
    			<button type="submit" class="btn btn-primary">Lưu thông tin</button>
    		</form>
    	</div>

    	<?php include_once('layouts/footer.php') ?>