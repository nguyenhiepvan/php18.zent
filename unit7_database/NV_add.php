<?php 
session_start();
include_once('layouts/header.php');
include_once('file_upload.php');
include_once('connection_database.php');
$_SESSION['staff']['PHOTOS'] = 'untitled.png';
?>
<?php  

  if(isset($_POST['submit'])){ // kiểm tra xem button Submit đã được click hay chưa ? 
  	$uploads = file_upload("images/staff","PHOTOS",500000,array( 'JPG', 'JPEG', 'GIF', 'PNG' , 'SVG','jpg', 'jpeg', 'gif', 'png' , 'svg'));
  	if(gettype($uploads) == "array"){
            print_r($uploads); // Trả về mảng lỗi nếu ko upload được
        }else{

            $_SESSION ['staff'] ['PHOTOS'] = $uploads; // Trả về tên file nếu upload thành công
        }
    }

    $query = "SELECT NAME_TYPE FROM type_nhan_vien";
    $result = $conn->query($query);
    ?>
    <div class="container">
    	<h3 align="center">THÊM MỚI NHÂN VIÊN</h3>
    	<hr>

    	<div style="overflow: auto;">
    		<img src="images/staff/<?=$_SESSION ['staff'] ['PHOTOS']?>" width = "100px" height = "100px" style= "float:left">
    		<form action="#" method="post" enctype="multipart/form-data" style="float: left; padding-left: 30px;padding-top: 30px;">

    			<input type="file" class="form-control" name="PHOTOS" style="width: 150px">
    			<input type="submit" value="Chose this photo" name="submit">
    		</form>

    	</li>
    </div>	
    <form action="NV_add_process.php" method="POST" role="form" enctype="multipart/form-data">

    	<div class="form-group">
    		<label for="">Tên Nhân viên</label>
    		<input type="text" class="form-control" id="" placeholder="Nhập vào tên hân viên" name="NAME">
    	</div>  
    	<div class="form-group">
    		<label for="">Số điện thoại</label>
    		<input type="number" class="form-control" id="" placeholder="Nhập vào số điện thoại" name="PHONE_NUMBER">
    	</div>
    	<div class="form-group">
    		<label for="">email</label>
    		<input type="email" class="form-control" id="" placeholder="Nhập vào email" name="EMAIL">
    	</div>
    	<div class="form-group">
    		<label for="">Địa chỉ</label>
    		<input type="text" class="form-control" id="" placeholder="Nhập vào địa chỉ" name="ADRESS">
    	</div>
    	<div class="form-group">
    		<label for="">Ngày sinh</label>
    		<input type="date" data-date="" data-date-format="yyyy-MM-dd" class="form-control" id="" name="DATE_OF_BIRTH">
    	</div>
    	<div class="form-group">
    		<label for="">Loại nhân viên</label>
    		<select name="TYPE">
    			<?php while ($row = $result->fetch_assoc()) {
					?>
					<option value="<?=$row['NAME_TYPE']?>"><?=$row['NAME_TYPE']?></option>
					<?php } ?>
    		</select> 
    	</div>
    	<button type="submit" class="btn btn-primary">Lưu thông tin</button>
    </form>
</div>

<?php include_once('layouts/footer.php') ?>